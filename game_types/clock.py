import time as lib_time


class Clock():

    def __init__(self, speed=0.0000001):
        self._time = 0
        self.speed = speed

    def tick(self):
        self.time += 1
        lib_time.sleep(self.speed)

    @property
    def time(self):
        return self._time

    @time.setter
    def time(self, value):
        self._time = value
