from random import random
from numpy import random

# select random squad
def random_strategy(army):
    return random.choice(army.squads)


# select army with the lowest HP value
def weakest_strategy(army):
    lowest_health = min([squad.total_health()for squad in army.squads])
    for squad in army.squads:
        if squad.total_health() == lowest_health:
            return squad


# select army with the biggest HP value
def strongest_strategy(army):
    highest_health = max([squad.total_health() for squad in army.squads])
    for squad in army.squads:
        if squad.total_health() == highest_health:
            return squad