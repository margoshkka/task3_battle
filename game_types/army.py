import game_types.strategy as strategy
from game_types.squad import Squad
from helper import ROUND_SIGNS


class Army:
    def __init__(self,clock, name, strategy_name, squads):
        self.is_alive = True
        self.squads = []
        self.clock = clock
        self.name = name
        try:
            self.strategy = getattr(strategy, strategy_name)
        except AttributeError:
            print('Can not create army with strategy ' + strategy_name)
            print('Army was created with RandomStrategy by default')
            self.strategy = strategy.random_strategy

        Squad.squads_counter = 0
        for squad in squads:
            self.add_squad(Squad(clock,len(squad.soldiers),len(squad.vehicles)))

    @property
    def score(self):
        score = 0
        for squad in self.squads:
            score += squad.score
        return score

    def add_squad(self, squad):
        self.squads.append(squad)

    def die_squads(self):
        for squad in self.squads:
            if not squad.is_alive:
                self.squads.remove(squad)
        if len(self.squads) == 0:
            self.is_alive = False

    def total_health(self):
        total_health = sum([squad.total_health() for squad in self.squads])

        if total_health <= 0:
            self.is_alive = False
        return round(total_health, ROUND_SIGNS)

    def print_info(self, flag=0):
        result = '______________'
        result += self.name + ' (' + self.strategy.__name__ + ')'
        result += '______________' + '\n'
        for squad in self.squads:
            result += squad.print_info(flag)
        return result

    def attack_enemy(self, enemy):
        result = self.name + ' (Health = ' + str(self.total_health()) + ') VS ' + \
                 enemy.name + ' (Health = ' + str(enemy.total_health()) + ')\n'
        for attacking_squad in self.squads:
            if len(enemy.squads) == 0:
                enemy.die_squads()
                break

            defending_squad = self.strategy(enemy)
            attacking_squad_probability = attacking_squad.take_damage()
            defending_squad_probability = defending_squad.take_damage()
            result += '\t' + attacking_squad.name + ' VS ' + defending_squad.name + ' => '
            if attacking_squad_probability > defending_squad_probability:
                damage = attacking_squad.get_damage()
                defending_squad.lost(damage)
                attacking_squad.won(damage)
                result += ' Round Win (' + str(attacking_squad_probability) + '/' + str(defending_squad_probability) + '),'
                result += ' Damage = ' + str(damage)+ ', Health =' + str(attacking_squad.total_health())
                result += ', Enemy`s Health =' + str(defending_squad.total_health()) + '\n'
                enemy.die_squads()
            else:
                result += ' Round Lost '
                result += self.name +', Health =' + str(attacking_squad.total_health())
                result += ', Enemy`s Health =' + str(defending_squad.total_health()) + ')\n'

        return result