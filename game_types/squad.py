#
# ## Squads
#
# Squads are consisted out of a number of units (soldiers or vehicles), that behave as a coherent group.
#
# A squad is active as long as is contains an active unit.
#
# #### Attack
#
# The attack success probability of a squad is determined as the geometric average o the attack success probability of each member.
#
# #### Damage
#
# The damage received on a successful attack is distributed evenly to all squad members.
# The damage inflicted on a successful attack is the accumulation of the damage inflicted by each squad member.
#
# ## Attacking & Defending
#
# Each time a squad attacks it must choose a target squad, depending on the chosen strategy:
#
#
# | Strategy  |              Description              |
# |-----------|---------------------------------------|
# | random    | attack any random squad               |
# | weakest   | attack the weakest opposing squad     |
# | strongest | attack the strongest opposing squad   |
#
# Once the target is determined both the attacking and defending squads calculate their attack probability success and the squad with the highest probability wins.
# If the attacking squad wins, damage is dealt to the defending side, otherwise no damage is inflicted to the attacking side.
#
# ------If the attacking squad wins, damage is dealt to the defending side.
# ------If the attacking squad loses, no damage is dealt to either side.
from game_types.soldier import Soldier
from game_types.vehicle import Vehicle
from helper import ROUND_SIGNS, geo_mean


class Squad():
    squads_counter = 0

    def __init__(self):
        self.score = 0

    def __init__(self,clock, soldiers_count, vehicles_count):
        self.clock = clock
        self.soldiers = []
        self.vehicles = []
        self.add_units(soldiers_count, vehicles_count)
        self.__class__.squads_counter += 1
        self.name = "Squad " + str(self.__class__.squads_counter)
        self.score = 0
        self.is_alive = True

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, value):
        if value <= 0:
            self.is_alive = False
        self._health = round(value, ROUND_SIGNS)

    @property
    def all_units(self):
        all = []
        all.extend(self.soldiers)
        all.extend(self.vehicles)
        return all

    def add_units(self, soldiers_count, vehicles_count):
        for _ in range(soldiers_count):
            self.soldiers.append(Soldier(self.clock))
        for _ in range(vehicles_count):
            self.vehicles.append(Vehicle(self.clock,3))

    def take_damage(self):
        total_attack = [soldier.take_damage() for soldier in self.soldiers]
        total_attack.extend([vehicle.take_damage() for vehicle in self.vehicles])
        return round(geo_mean(total_attack), ROUND_SIGNS)

    def get_damage(self):
        total_damage = 0
        for one_unit in self.all_units:
            total_damage += one_unit.get_damage()
        return round(total_damage, ROUND_SIGNS)

    def die_units(self):
        for soldier in self.soldiers:
            if not soldier.is_alive:
                self.soldiers.remove(soldier)
        for vehicle in self.vehicles:
            if not vehicle.is_alive:
                self.vehicles.remove(vehicle)
        if len(self.all_units) == 0:
            self.is_alive = False

    def total_health(self):
        total_health = sum([one_unit.health for one_unit in self.all_units])
        return round(total_health, ROUND_SIGNS)

    def print_info(self, flag=0):
        result = self.name + " Health=" + str(self.total_health()) + '\n'
        if flag == 1:
            for one_unit in self.all_units:
                result += one_unit.print_info()
        return result

    def lost(self, damage):
        count_units = len(self.all_units)
        for one_unit in self.all_units:
            one_unit.lost(round(damage / count_units, ROUND_SIGNS))
        self.die_units()
        if self.total_health() <= 0:
            self.is_alive = False

    def won(self, damage):
        self.score += damage * 100
        for one_unit in self.all_units:
            one_unit.won()

