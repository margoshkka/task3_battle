import logging


class Logger():
    def __init__(self, filename="data/logs.log", flag='w+'):
        self.f = open(filename, flag)

    def log(self, str):
        self.f.write(str)

    def end_log(self):
        self.f.close()