#
# ### Soldiers
#
# Soldiers are units that have an additional property:
#
#
# |  Property  |   Range  |            Description            |
# |------------|----------|-----------------------------------|
# | experience | \[0-50\] | Represents the soldier experience |
#
# The **experience**  property is incremented after each
# successful attack, and is sed to calculate the attack
# success probability and the amount of damage inflicted
#
# Soldiers are considered active as long as they have any health.
#
# #### Attack
#
# Soldiers attack success probability is calculated:
#
# 0.5 * (1 + health/100) * random(50 + experience, 100) / 100
#
# where **random(min, max)** returns a random number between min and max (inclusive)
#
# #### Damage
#
# The amount of damage a soldier can afflict is calculated as
# follows:
#
# 0.05 + experience / 100

from game_types.unit import BaseUnit
from game_types.logger import Logger
from helper import ROUND_SIGNS
import random

class Soldier(BaseUnit):
    soldiers_counter = 0
    def __init__(self, clock):
        self._health = 50
        self.clock = clock
        self._is_alive = True
        self.experience = 0
        self.__class__.soldiers_counter += 1
        self.__class__.soldiers_counter += 1
        self.__recharge = random.randint(1, 2)
        self.name = "Soldier " + str(self.__class__.soldiers_counter)
        self._last_attack_time = self.clock.time

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, value):
        if value <=0:
            self.is_alive = False
        self._health = round(value, ROUND_SIGNS)

    @property
    def is_alive(self):
        return self._is_alive

    @is_alive.setter
    def is_alive(self,value):
        _is_alive = value

    def get_damage(self):

        return round(0.05 + self.experience / 100, ROUND_SIGNS)

    # if soldier is recharging
    def get_rest(self):
        return self._last_attack_time < self.clock.time

    def take_damage(self):
        '''
        damage that he gives to the opposite
        '''
        # if self.get_rest():
        #     return 0
        try:
            random_coefficient = random.randrange(50 + self.experience, 100+1) / 100
        except ValueError:
            random_coefficient = 0.5
        self._last_attack_time = self.clock.time+ self.__recharge
        return round(0.5 * (1 + self.health / 100) * random_coefficient, ROUND_SIGNS)

    def won(self):
        self.increase_experience(1)

    def lost(self, damage):
        self.health -= damage
        log_str = ''
        log_str += (self.name + " damage taken: " + str(damage) + ' at ' + str(self.clock.time) + '\n')
        log_str += "HP left: " + str(self.health) + '\n'
        log_str += "Exp: " + str(self.experience) + '\n'
        logger = Logger('actions.log', 'a')
        logger.log(log_str)

    def increase_experience(self, point):
        if self.experience >=50:
            return
        self.experience += point

    def print_info(self, name=''):
        if name == '':
            name = self.name
        return '\t' + name + ' ' + str(self.health) + ' ' + str(self.experience) + '\n'


