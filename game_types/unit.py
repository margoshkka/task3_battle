#
# ## Units
#
# Each unit represents either a soldier or a vehicle maned by a predetermined number of soldiers.
#
# All units have the following properties:
#
#
# | Property |     Range    |                             Description                                 |
# |----------|--------------|-------------------------------------------------------------------------|
# | health   | % \[0-100\]  | Represents the health of the unit                                       |
# | recharge | \[100-2000\] | Represents the number of ms required to recharge the unit for an attack |
#

from abc import ABCMeta,abstractmethod


class BaseUnit(metaclass=ABCMeta):
    # def __init__(self):
    #     self.health = 100

    @property
    @abstractmethod
    def health(self):
        pass

    @health.setter
    @abstractmethod
    def health(self,value):
        pass

    @property
    @abstractmethod
    def is_alive(self):
        pass

    @abstractmethod
    def get_damage(self):
        pass

    @abstractmethod
    def take_damage(self):
        pass





