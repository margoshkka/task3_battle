# ## Vehicles
#
# A battle vehicle has these additional properties:
#
#
# |   Property  |  Range  |                        Description                     |
# |-------------|---------|--------------------------------------------------------|
# |  operators  | \[1-3\] | The number of soldiers required to operate the vehicle |
#
# The **recharge** property for a vehicle must be
# greater than 1000 (ms).
#
# The total health of a vehicle unit is represented as the
# average health of all it's operators and
# the health of the vehicle.
#
# A vehicle is considered active as long as it self has any
# health and there is an vehicle operator with any health.
# If the vehicle is destroyed, any remaining vehicle operator
# is considered as inactive (killed).
#
#
# #### Attack
# The Vehicle attack success probability is determined as follows:
#
# 0.5 * (1 + vehicle.health / 100) * gavg(operators.attack_success)
#
# where **gavg** is the geometric average of the
#  attack success of all the vehicle operators
#
# #### Damage
#
# The damage afflicted by a vehicle is calculated:
#
# 0.1 + sum(operators.experience / 100)
#
# The total damage inflicted on the vehicle is distributed to the operators as follows:
# 60% of the total damage is inflicted on the vehicle
# 20% of the total damage is inflicted on a random vehicle operator
# The rest of the damage is inflicted evenly to the other operators (10% each)

from game_types.unit import BaseUnit
from game_types.soldier import Soldier
from game_types.logger import Logger
from helper import ROUND_SIGNS,geo_mean

from random import random
from numpy import random

class Vehicle(BaseUnit):
    vehicle_counter = 0
    def __init__(self,clock,operators_count):
        self.clock = clock
        self.operators = []
        self.set_operators(operators_count)
        self._is_alive = True
        self._health = 50
        self.__class__.vehicle_counter += 1
        self.name = "Vehicle " + str(self.__class__.vehicle_counter)

    def set_operators(self, operators_count):
        Soldier.soldiers_counter = 0
        for i in range(0,operators_count):
            self.operators.append(Soldier(self.clock))

    @property
    def health(self):
        healthes = [i.health for i in self.operators] # health of every operator
        return self._health + sum(healthes)/len(healthes)

    @health.setter
    def health(self, value):
        if value <= 0:
            self.is_alive = False
        self._health = round(value, ROUND_SIGNS)

    @property
    def is_alive(self):
        return self._is_alive

    @is_alive.setter
    def is_alive(self, value):
        self._is_alive = value

    def die_operator(self):
        for operator in self.operators:
            if not operator.is_alive:
                self.operators.remove(operator)
        if len(self.operators) == 0:
            self.is_alive = False

    def get_damage(self):
        sum_exp = sum([operator.experience / 100 for operator in self.operators])
        return round(0.1 + sum_exp, ROUND_SIGNS)

    def take_damage(self):
        '''
        damage that he gives to the opposite
        '''
        attacks_value = [operator.take_damage() for operator in self.operators]
        return round(0.5 * (1 + self.health / 100) * geo_mean(attacks_value),ROUND_SIGNS)


    def lost(self, damage):
        self.health = self._health - damage*0.6
        random_operator = random.choice(self.operators)
        random_operator.health = random_operator.health - damage*0.2
        for operator in self.operators:
            if operator != random_operator:
                operator.health = operator.health - damage * 0.1
        self.die_operator()
        log_str = ''
        log_str += (self.name + " damage taken: " + str(damage) + ' at ' + str(self.clock.time) + '\n')
        log_str += "HP left: " + str(self.health) + '\n'
        for operator in self.operators:
            log_str += '\t' + '\toperator ' + str(operator.health) + ' ' + str(operator.experience) + '\n'
        logger = Logger('actions.log', 'a')
        logger.log(log_str)

    # def print_info(self):
    #     result = '\t' + self.name + '\n'
    #     for operator in self.operators:
    #         result += '\t' + '\t ' + str(operator.health) + ' ' + str(operator.experience) + '\n'
    #     return result

    def won(self):
        [operator.increase_experience(1) for operator in self.operators]