from turtledemo import clock

from game_types.army import Army
from game_types.squad import Squad
from game_types.logger import Logger
from game_types.clock import Clock
from numpy.random import choice
import json


def main():
    clock = Clock()
    logger = Logger()
    logger.log(str(clock.time))
    armies = []
    val = input('if u wanna watch json-based fight - print "j"\nor if u wanna use my data print "d"\n')

    if val == 'j':
        data = None
        with open('data/my_war.json') as my_json:
            data = json.load(my_json)
        for arm in data["armies"]:
            armies.append(Army(clock, arm["name"],arm["strategy"],
                               [Squad(clock,len([unit for unit in squad["units"]
                                                 if unit["unit_type"]=="Soldier"]),
                                      len([unit for unit in squad["units"]
                                           if unit["unit_type"]=="Vehicle"]))
                                for squad in arm["squads"]]))
    else:
        print('something wrong with json so using my data')

        army_USA = Army(clock,'USA', 'random_strategy',[Squad(clock,5,1),Squad(clock,6,2),Squad(clock,6,5)])
        army_China = Army(clock,'China', 'weakest_strategy',[Squad(clock,5,1),Squad(clock,6,2),Squad(clock,6,5)])
        army_Korea = Army(clock,'Korea', 'strongest_strategy',[Squad(clock,5,1),Squad(clock,6,2),Squad(clock,6,5)])
        armies.extend([army_USA,army_China,army_Korea])
        pass

    while True:
        current_army = choice(armies)
        enemy_armies = [enemy for enemy in armies
                        if enemy is not current_army and enemy.is_alive
                        ]
        if len(enemy_armies) == 0:
            print('winner: ' + current_army.name)
            for army in armies:
                game_result = army.print_info()
                print(game_result)
                logger.log(game_result)
            exit()
        else:
            target = choice(enemy_armies)
            round_result = current_army.attack_enemy(target)
            logger.log(round_result)
            print(round_result)
        clock.tick()

if __name__ == '__main__':
    main()