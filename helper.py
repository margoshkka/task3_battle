import numpy

ROUND_SIGNS = 4


def geo_mean(num_list):
    np_array = numpy.array(num_list)
    return np_array.prod() ** (1.0 / len(np_array))